# DAMA-container

Welcome to DAMA container repository.

DAMA is a protein structure multiple alignment tool which is independent of rigid-body
constraints.

To learn more, see the original paper
[here](https://doi.org/10.1093/bioinformatics/btab571).

Online (limited) service is located
[here](https://dworkowa.imdik.pan.pl/EP/DAMA/).

Requirements:
* Machine with CUDA GPU
* Docker Desktop

Steps to use:
* login to docker repository (note, that, depending on your Docker Desktop installation
you might need to add `sudo` at the beggining of each docker command):
```bash
docker login -u gitlab+deploy-token-980051 -p Bjngu2DU2GxuVAKt4Y5Q registry.gitlab.com/vdhert/dama-container/
```
* pull the container:
```bash
docker pull registry.gitlab.com/vdhert/dama-container/dama
```
* run the container:
```bash
docker run --rm -v <working-directory>:/dama registry.gitlab.com/vdhert/dama-container/dama <args>
```
replacing `<args>` with input and `<working-directory>` with path to working directory,
where results or inputfiles are (to be stored).
In most cases workig directory should be `$(pwd)` (current working directory).

Inputs are:
* any number of PDB ids or paths to `.pdb` files to be compared
* prefix for result files

Examples:
1) local files only (assuming files `a.pdb` and `b.pdb` in `~/my-first-run` directory):
```bash
cd ~/my-first-run
docker run --rm -v $(pwd):/dama registry.gitlab.com/vdhert/dama-container/dama a.pdb b.pdb dama-results
ls
```
Running above commands will result in:
```
a.pdb
b.pdb
dama-results_0.csv
dama-results_0.fasta
dama-results_0.mal
dama-results_0.nh
dama-results_0.pal
dama-results_0.pdb
dama-results_0.scr
dama-results_0.tree
```
1) PDB ids only
```bash
cd ~/my-first-run
docker run --rm -v $(pwd):/dama registry.gitlab.com/vdhert/dama-container/dama 1luf 2src res
```
will result with resulting files starting with `res` prefix.
